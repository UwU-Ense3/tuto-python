from tkinter import*
from random import randrange

################################################################


def il_neige(profil,position):
    profil[position]+=1



def est_instable(profil):
    lg=len(profil)
    for i in range(lg-1):
                  if profil[i]>profil[i+1]+2:
                      return True

    if profil[lg-1]>2:
        return True

    for i in range (1,lg):
        if profil[i]>profil[i-1]+2:
            return True
    return False



def va_tomber(profil,position):
                   prolong=profil*1
                   prolong.append(0)
                   if prolong[position]>prolong[position+1]+2:
                       return(True,True)
                   if position > 0:
                       return(prolong[position]>prolong[position-1]+2,False)
                   else:
                       return(False,False)



def un_glissement(profil):
    i=0
    while i<len(profil) and va_tomber(profil,i)==(False, False):
        i+=1
    if i==len(profil):
        return 0
    elif va_tomber(profil,i)==(True,True):
        profil[i]=profil[i]-2
        if i==len(profil)-1:
            return 2
        else:
            profil[i+1]+=2
            return 0
    else:
        profil[i]=profil[i]-2
        profil[i-1]+=2
        return 0

################################################################
def avalanche():
    global c
    if est_instable(profil):
        c=c+un_glissement(profil)
        dessin(c)
        print(profil)
        print (c)
        txt.config(text=c)
        dess.after(cu.get(),avalanche)
################################################################
def echap():
    
    fen.destroy()


    
def dessin(c):
    dess.delete(ALL)
    for i in range (len(profil)):
        dess.create_rectangle(lc*i,700-h*profil[i],lc*(i+1),700,fill='white',width=1)
    dess.create_rectangle(lc*(len(profil)),700-h*c,lc*(len(profil)+1),700,fill='red',width=1)



################################################################
profil=[]
n=randrange(80,150)
for i in range (n-50):
    profil.append(randrange(int((n-i)-50),n-i))
    print(profil)
    
#profil=([25,32,27,21,24,26,35,15,14,13,12,10,30,15,16,14,25,26,21,23,24,20,19,18,17,15,10,12,13,12,10,12,12,12,13,10,10,9,8,7,7,7,7,7,7,4,2,3,1,5,6,3,2,1])

#profil=[5,3,2,1,4,0]

################################################################
lc=1100/(len(profil)+1)
h=700/(max(profil)+1)
c=0

################################################################


fen=Tk()
fen.title('montagne')
fen.resizable(width=YES,height=YES)
fen.minsize(width=800,height=800)
fen.maxsize(width=None,height=None)
fen.wm_state(newstate='zoomed')


dess=Canvas(fen,width=1100,height=700,bg='cyan',bd=5,relief='ridge')
dess.place(x=10,y=10)

B1=Button(fen, text='Echap',anchor=CENTER,bg='#6a9662',font=('Old English Text MT',30,"bold"),command=echap)
B1.place(x=1200,y=10)

          
B2=Button(fen, text='Start',anchor=CENTER,bg='#6a9662',font=('Old English Text MT',30,"bold"),command=avalanche)
B2.place(x=1200,y=100)

cu=Scale(fen,from_=1,to=150, resolution = 1,label='tps')
cu.place(x=1200,y=300)
cu.set(100)

txt=Label(fen, text=c,bg='#6a9662',fg='black',font=('Old English Text MT',30,"bold"))
txt.place(x=1100,y=730)

################################################################
for i in range (len(profil)):
    dess.create_rectangle(lc*i,700-h*profil[i],lc*(i+1),700,fill='white',width=1)
fen.mainloop()
